package com.learnkafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Kafka4devsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Kafka4devsApplication.class, args);
	}

}
